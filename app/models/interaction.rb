class Interaction < ApplicationRecord
  belongs_to :user_one, class_name: 'User'
  belongs_to :user_two, class_name: 'User'

  # Juan le dio 'Like' a Sandra
  # Necesitamos saber si Sandra le dió 'like' a Juan
  before_save :check_match # Antes de guardarse, verificar si existe match o no.
  def check_match
    sender = user_one
    reciever = user_two
    # Si interacción existe entre el emisor y receptor y viceversa,
    # Se generará un match
    if Interaction.where(user_one: reciever, 
                          user_two: sender, 
                          like: true ).any?
      Match.create(user_one: sender, user_two: reciever)
    end
  end
end
