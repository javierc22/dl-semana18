# Semana 18

### SCRUM - Agilidad

1. Intro a agilidad
2. Waterfall
3. Intro a Trello
4. Una buena historia
5. La primera historia
6. Más historias
7. Trello
8. Planing poker
9. PLanificación
10. Primera historia Parte 1
11. Primera historia Parte 2
12. Primera historia Parte 3
13. Intro a Testing
14. Primeros Test automatizados
15. Historia de interacción
16. Guardando la interacción
17. Relaciones con nombre
18. Controller para las interacciones
19. Guardando Like y Dislike
20. Verificando la segunda historia de usuario
21. Tercera historia. Modelo y relaciones del Match
22. Lógica de negocio
23. Verificación de matches
24. Preparando el Next Sprint

Documentación de Testing

http://guides.rubyonrails.org/testing.html


# Proyecto Tinder

### Historia de usuario 1

Yo como visita ingreso al registro de usuarios y en el formulario debo escribir mi nombre, email, password, una bio y una foto. El nombre, bio y foto compondrá un perfil que otros usuarios podrán ver, luego seré redirigido a un perfil al azar.

### Historia de usuario 2

Yo al entrar como usuario observo un perfil de otro usuario escogido al azar y puedo marcar que me gusta este perfil o no me gusta, para intentar obtener un match, después soy redirigido a otro perfil al azar.

### Historia de usuario 3

Yo como usuario al dar like a un usuario que ya me ha dado like genero un match, los matches permiten conversar con la persona gustada.

### Historia de usuario 4

Yo como usuario entro a la lista de matches y puedo ver todos los usuarios con los que he hecho match, desde esta lista puedo seleccionarlos y ver el perfil de uno de ellos.

### Historia de usuario 5

Yo como usuario entro al perfil de otro usuario con el que ya hice match y puedo conversar con el para invitarlo a salir.

### Modelo físico:

![](modelo_fisico.png)