class UsersController < ApplicationController
  before_action :authenticate_user!

  def search
    @user = User.random_user(current_user.id)
  end
end
