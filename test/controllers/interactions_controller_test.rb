require 'test_helper'

class InteractionsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  # test "the truth" do
  #   assert true
  # end

  test 'user should interact with others' do
    current_user = users(:one) # Current_user es igual a ":one" de Fixtures Users.yml
    sign_in(current_user) # Iniciar sesión con current_user
    liked_user = users(:two) # liked_user (Usuario gustado) es igual a ":two" de Fixtures Users.yml 

    assert_difference('Interaction.count') do # Comprobar diferencia en la cantidad de interacciones
      post interactions_path, 
      params: { user_two_id: liked_user.id, like: true } # Crear interacción entre current_user y liked_user.
    end

    assert_redirected_to users_search_path # Comprueba redireccionamineto a users_search_path
  end
end
