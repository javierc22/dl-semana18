require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  # test "the truth" do
  #   assert true
  # end
  test 'visit should be logged to access root page' do
    # Al entrar a la página principal... Debo ser redirigido
    get root_path
    assert_response :redirect
  end

  test 'user can access to root page' do
    user = users(:one) # 'user' es igual a ':one' de 'users.yml' 
    sign_in(user) # iniciar sesión con 'user'
    get root_path # ingresar a la página principal
    assert_response :success
  end
end
