require 'test_helper'

class InteractionTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test 'if both users like each other then generate a match' do
    sender = users(:one) # 'sender' es igual a ':one' de 'users.yml' 
    reciever = users(:two) # 'reciever' es igual a ':two' de 'users.yml'

    # Generar una nueva interacción con like: true y guardarla
    interaction = Interaction.new(user_one: reciever, user_two: sender, like: true).save!
    assert_equal 1, Match.count # Se espera que se genere 1 Match
  end
end
